//WAP to find the volume of a tromboloid using 4 functions.
#include  <stdio.h>
float input()
{
    float n;
    
    scanf("%f",&n);
    return n;
}

float compute(float num1,float num2,float num3)
{
    float v = (1/(3*num1))*((num2*num3)+num3);
    return v;
}

void display(float vol)
{
    printf("volume of the tromboloid of given dimensions is = %f",vol);
}

int main()
{
    float vol,b,d,h;
    printf("enter the value of b,h,d respectively = ");
    b = input();
    h = input();
    d = input();
    vol = compute(b,h,d);
    display(vol);
	return 0;
}


    



