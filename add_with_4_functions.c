//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
float input()
{
    float n;
    printf("enter the number to add:");
    scanf("%f",&n);
    return n;
}

float add(float n1,float n2)
{
    float c = n1+n2;
    return c;
}

void print(float n3,float n4,float n5)
{
    printf("%f + %f = %f",n3,n4,n5);
}

int main()
{
    float x,y,z;
    x = input();
    y = input();
    z= add(x,y);
    
    print(x,y,z);
}
